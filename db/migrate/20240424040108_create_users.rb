class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.string :username, limit: 20, null: false
      t.string :password_digest, null: false
      t.text :bio, limit: 150
      t.timestamps
    end

    add_index :users, :username, unique: true
  end
end
