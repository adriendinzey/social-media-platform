class CreateFriendships < ActiveRecord::Migration[6.0]
  def change
    create_table :friendships do |t|
      t.references :user, foreign_key: { to_table: :users }, null: false
      t.references :friend, foreign_key: { to_table: :users }, null: false
      t.boolean :status, default: false, null: false
      t.boolean :requester_id, default: false, null: false

      t.timestamps
    end

    add_index :friendships, [:user_id, :friend_id], unique: true
  end
end
