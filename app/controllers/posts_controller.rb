class PostsController < ApplicationController
  def create
    @post = Post.new(post_params)
    @post.author = current_user

    respond_to do |format|
      if @post.save
        format.turbo_stream { render turbo_stream: turbo_stream.append("posts", @post) }
        format.html { redirect_to root_path, notice: "Post was successfully created." }
      else
        format.turbo_stream { render turbo_stream: turbo_stream.replace("new_post_form", partial: "shared/new_post", locals: { post: @post }) }
        format.html { render :new }
      end
    end
  end

  private

  def post_params
    params.require(:post).permit(:content)
  end
end
