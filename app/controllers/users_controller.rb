class UsersController < ApplicationController
  before_action :set_user, only: [:show, :destroy, :update_password, :update_username, :update_bio, :update_profile_picture]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to @user, notice: 'User was successfully created.'
    else
      flash.now[:alert] = 'Failed to create user. Ensure all fields are filled and passwords match.'
      render :new, status: :unprocessable_entity
    end
  end

  def show
    @user = User.find(params[:id])
    @posts = @user.posts.order(created_at: :desc).page(params[:page])
    @friends = @user.friends
  end

  def index
    @users = User.all
  end

  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.'
  end

  def search
    @query = params[:query]
    @users = User.where('username LIKE ?', "%#{@query}%")
  end

  def update_password
    if @user&.authenticate(params[:current_password])
      if @user.update(password: params[:new_password])
        redirect_to @user, notice: 'Password was successfully updated.'
      else
        flash.now[:alert] = 'Error updating password.'
        render :show, status: :unprocessable_entity
      end
    else
      flash.now[:alert] = 'Error updating password.'
      render :show, status: :unprocessable_entity
    end
  end

  def update_username
    if @user.update(username: params[:new_username])
      redirect_to @user, notice: 'Username was successfully updated.'
    else
      flash.now[:alert] = 'Error updating username.'
      @posts = @user.posts.order(created_at: :desc).page(params[:page])
      render :show
    end
  end

  def update_bio
    if @user.update(bio_params)
      redirect_to @user, notice: 'Bio was successfully updated.'
    else
      flash.now[:alert] = 'There was an error updating your bio.'
      print("error")
      @posts = @user.posts.order(created_at: :desc).page(params[:page])
      render :show
    end
  end

  def update_profile_picture
    if @user.update(profile_picture: params[:user][:profile_picture])
      redirect_to @user, notice: 'Profile picture was successfully updated.'
    else
      flash.now[:alert] = 'There was an error updating your profile picture.'
      render :show
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:username, :password, :password_confirmation, :bio, :profile_picture)
  end

  def bio_params
    params.require(:user).permit(:bio)
  end
end
