class FriendshipsController < ApplicationController
  before_action :set_friend_create, only: [:create]
  before_action :set_friend, only: [:destroy,:update]

  def create
    existing_request = Friendship.find_by(user_id: current_user.id, friend_id: @friend.id, status: :pending)
    reverse_request = Friendship.find_by(user_id: @friend.id, friend_id: current_user.id, status: :pending)

    if existing_request || reverse_request
      Rails.logger.debug "#{existing_request}"
      Rails.logger.debug "#{reverse_request}"
    else
      Friendship.create(user_id: current_user.id, friend_id: @friend.id, status: :pending, requester_id: :user1)
      Friendship.create(user_id: @friend.id, friend_id: current_user.id, status: :pending, requester_id: :user2)
      redirect_to @friend, notice: 'Friend request sent.'
    end
  end

  def destroy
    if current_user.friends.exists?(@friend.id)
      current_user.friends.destroy(@friend)
      @friend.friends.destroy(current_user)
      redirect_to @friend, notice: 'Friend removed.'
    elsif current_user.friendships.exists?(friend: @friend)
      cancel_friend_request
    end
  end

  def update
    Rails.logger.debug "#{@friend.id}"
    Rails.logger.debug "#{current_user.id}"
    if current_user.friendships.exists?(friend: @friend)
      accept_friend_request
    end
  end

  private
    def set_friend_create
      @friend = User.find(params[:friendship][:friend_id])
    end

    def set_friend
      @friend = User.find(params[:id])
    end

    def cancel_friend_request
      current_user.friendships.find_by(friend: @friend).destroy
      @friend.friendships.find_by(friend: current_user).destroy
      redirect_to @friend, notice: 'Friend request canceled.'
    end

    def accept_friend_request
      friendship = current_user.friendships.find_by(friend: @friend)
      friendship.update(status: :accepted)
      friendship2 = @friend.friendships.find_by(friend: current_user)
      friendship2.update(status: :accepted)
      redirect_to @friend, notice: 'Friend request accepted.'
    end
end
