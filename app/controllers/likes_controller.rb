class LikesController < ApplicationController
  before_action :find_likeable_post, only: [:like_post, :unlike_post]
  before_action :find_likeable_comment, only: [:like_comment, :unlike_comment]

  def like_post
    @likeable.likes.create(user: current_user)
    respond_to do |format|
      format.html { redirect_to @likeable }
      format.turbo_stream { render turbo_stream: turbo_stream.replace("post_#{@likeable.id}_likes",
      partial: "likes/likeable", locals: { likeable: @likeable }) }
    end
  end

  def unlike_post
    @likeable.likes.find_by(user: current_user)&.destroy
    respond_to do |format|
      format.html { redirect_to @likeable }
      format.turbo_stream { render turbo_stream: turbo_stream.replace("post_#{@likeable.id}_likes",
      partial: "likes/likeable", locals: { likeable: @likeable }) }
    end
  end

  def like_comment
    @likeable.likes.create(user: current_user)
    respond_to do |format|
      format.html { redirect_to @likeable.post }
      format.turbo_stream { render turbo_stream: turbo_stream.replace("comment_#{@likeable.id}_likes",
      partial: "likes/likeable", locals: { likeable: @likeable }) }
    end
  end

  def unlike_comment
    @likeable.likes.find_by(user: current_user)&.destroy
    respond_to do |format|
      format.html { redirect_to @likeable.post }
      format.turbo_stream { render turbo_stream: turbo_stream.replace("comment_#{@likeable.id}_likes",
      partial: "likes/likeable", locals: { likeable: @likeable }) }
    end
  end

  private

  def find_likeable_post
    @likeable = Post.find(params[:id])
  end

  def find_likeable_comment
    @likeable = Comment.find(params[:id])
  end

end
