class HomeController < ApplicationController
  def index
    if current_user
      friend_posts = current_user.friends.map(&:posts).flatten.sort_by(&:created_at).reverse
      @posts = Kaminari.paginate_array(friend_posts).page(params[:page]).per(20)
    else
      render 'render/index'
    end
  end
end
