class Friendship < ApplicationRecord
  belongs_to :user
  belongs_to :friend, class_name: 'User', foreign_key: 'friend_id'

  validates :user_id, uniqueness: { scope: :friend_id, message: "Friendship already exists" }
  validates :status, presence: true
  validates :requester_id, presence: true, unless: -> { accepted? }
  validate :user_and_friend_are_different

  enum status: { pending: false, accepted: true }
  enum requester_id: { user1: false, user2: true }

  def user_and_friend_are_different
    errors.add(:base, "User and friend cannot be the same") if user_id == friend_id
  end
end
