class User < ApplicationRecord

  # Fields
  validates :username, presence: true, length: { maximum: 20 }, uniqueness: true
  validates :password, presence: true, on: :create
  validates :bio, length: { maximum: 150 }
  has_secure_password

  #Associations
  has_many :posts, foreign_key: 'author_id', dependent: :destroy
  has_many :comments, foreign_key: 'author_id', dependent: :destroy
  has_many :likes, dependent: :destroy

  has_many :friendships
  has_many :friends, -> { where(friendships: { status: :accepted }) }, through: :friendships, source: :friend
  has_many :friend_requests, -> { where(requester_id: :user1, status: :pending) }, class_name: 'Friendship', foreign_key: 'user_id'
  # Profile Picture Handling

  DEFAULT_PROFILE_PICTURE = "default_pfp.png"
  MAX_IMAGE_SIZE = 2.megabytes
  MAX_IMAGE_DIMENSION = 300

  has_one_attached :profile_picture
  before_create :set_default_profile_picture

  def friends_posts
    friend_ids = friends.pluck(:id)
    Post.where(author: friend_ids)
  end

  def accepted_friendships
    Friendship.where(user_id: id, status: :accepted)
  end

  def self.search(query)
    where("username LIKE ?", "%#{query}%")
  end

  private
  def set_default_profile_picture
    self.profile_picture.attach(io: File.open("#{Rails.root}/app/assets/images/#{DEFAULT_PROFILE_PICTURE}"), filename: DEFAULT_PROFILE_PICTURE, content_type: 'image/png') unless profile_picture.attached?
  end
end
