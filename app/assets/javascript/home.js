$(document).on('turbolinks:load', function() {
    $('#load-more-posts').on('ajax:success', function(event) {
      var data = event.detail[0];
      $('#posts').append(data);
      $('#load-more').html(data.load_more_link);
    }).on('ajax:error', function(event) {
        print(event)
        print("ajax:error")
    });
  });