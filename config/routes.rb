Rails.application.routes.draw do
  get 'posts/index'
  get 'render/index'
  # Create User Page
  get '/sign-up', to: 'users#new', as: 'sign-up'

  # Login Routes
  get 'login', to: 'sessions#new', as: 'login'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy', as: 'logout'
  # Users routes
  namespace :api do
    namespace :v1 do
      resources :posts
    end
  end

  resources :friendships, only: [:create, :destroy, :update]

  get '/search', to: 'users#search', as: 'search_users'

  resources :users, only: [:create, :show, :index, :destroy] do
    member do
      patch 'update_password'
      patch 'update_username'
      patch 'update_bio'
      patch 'update_profile_picture'
    end
  end

  resources :posts do
    resources :comments, only: [:create]
  end

  post 'posts/:id/like', to: 'likes#like_post', as: 'like_post'
  delete 'posts/:id/unlike', to: 'likes#unlike_post', as: 'unlike_post'

  post 'comments/:id/like', to: 'likes#like_comment', as: 'like_comment'
  delete 'comments/:id/unlike', to: 'likes#unlike_comment', as: 'unlike_comment'
  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  root "home#index"
end
