require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "POST #create" do
    context "with valid attributes" do
      it "creates a new user" do
        expect {
          post :create, params: { user: { username: "test_user", password: "password", password_confirmation: "password"} }
        }.to change(User, :count).by(1)
      end
      it "redirects to the created user" do
        post :create, params: { user: { username: "test_user2", password: "password", password_confirmation: "password"} }
        expect(response).to redirect_to(user_path(User.last))
      end
    end
    context "with invalid attributes" do
      it "does not create a new user" do
        expect {
          post :create, params: { user: { username: nil, password: "password", bio: "Test bio" } }
        }.not_to change(User, :count)
      end
      it "re-renders the new template" do
        post :create, params: { user: { username: nil, password: "password", bio: "Test bio" } }
        expect(response).to render_template(:new)
      end
    end
  end
  describe "GET #show" do
    it "assigns the requested user to @user" do
      user = FactoryBot.create(:user)
      session[:user_id] = user.id
      get :show, params: { id: user.id }
      expect(assigns(:user)).to eq(user)
    end
  end
  describe "DELETE #destroy" do
    it "destroys the requested user" do
      user = FactoryBot.create(:user)
      expect {
        delete :destroy, params: { id: user.id }
      }.to change(User, :count).by(-1)
    end

    it "redirects to the users index" do
      user = FactoryBot.create(:user)
      delete :destroy, params: { id: user.id }
      expect(response).to redirect_to(users_url)
    end
  end
end
