require 'rails_helper'

RSpec.describe Like, type: :model do
  fixtures :users
  describe "associations" do
    it { should belong_to(:user) }
    it { should belong_to(:likeable) }
  end

  describe "validations" do
    it "validates uniqueness of user_id within the scope of likeable_type and likeable_id" do
      user = User.create(username: "test", password: "password", bio: "Test bio")
      post = Post.create(author: user, content: "test post")
      like = build(:like, user: user, likeable: post)
      expect(like).to be_valid
    end
  end
end
