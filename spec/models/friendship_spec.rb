require 'rails_helper'

RSpec.describe Friendship, type: :model do
  fixtures :users
  fixtures :friendships
  describe "validations" do
    it { should validate_uniqueness_of(:user_id).scoped_to(:friend_id).with_message("Friendship already exists") }
    it { should validate_presence_of(:status) }
    it "validates presence of requester_id unless status is accepted" do
      friendship = Friendship.new(status: "pending")
      expect(friendship).to validate_presence_of(:requester_id)
    end
  end

  describe "associations" do
    it { should belong_to(:user) }
    it { should belong_to(:friend).class_name('User') }
  end
end
