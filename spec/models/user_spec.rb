require 'rails_helper'

RSpec.describe User, type: :model do
  fixtures :users
  describe "validations" do
    it { should validate_presence_of(:username) }
    it { should validate_length_of(:username).is_at_most(20) }
    it { should validate_uniqueness_of(:username) }
    it { should validate_presence_of(:password) }
    it { should validate_length_of(:bio).is_at_most(150) }
  end
  describe "associations" do
    it { should have_many(:friendships) }
    it { should have_many(:friends).through(:friendships) }
    it { should have_many(:friend_requests).class_name('Friendship').with_foreign_key('user_id').conditions(requester_id: :user1, status: :pending) }
    it { should have_many(:posts).dependent(:destroy) }
    it { should have_many(:comments).dependent(:destroy) }
    it { should have_many(:likes).dependent(:destroy) }
  end

  describe "profile picture handling" do
    it "sets default profile picture before creation" do
      user = User.create(username: "test", password: "password", bio: "Test bio")
      expect(user.profile_picture.filename).to eq(User::DEFAULT_PROFILE_PICTURE)
    end

    it "attatches a valid profile picture" do
      user = User.create(username: "test", password: "password", bio: "Test bio")

      Rails.logger.debug("Before attaching profile picture")
      file = File.open(Rails.root.join('spec', 'fixtures', 'test_image_correct.png'))
      user.profile_picture.attach(io: file, filename: 'test_image_correct.png', content_type: 'image/png')
      user.save

      Rails.logger.debug("Profile picture attached: #{user.profile_picture.attached?}")
      expect(user.profile_picture).to be_attached

      pfp = MiniMagick::Image.read(user.profile_picture.download)

      Rails.logger.debug("Profile picture dimensions: #{pfp.width}x#{pfp.height}")
      expect(pfp.width).to be <= User::MAX_IMAGE_DIMENSION
      expect(pfp.height).to be <= User::MAX_IMAGE_DIMENSION
      expect(user.profile_picture.blob.byte_size).to be <= User::MAX_IMAGE_SIZE

      expect(pfp.width).to eq(pfp.height)
      file.close
    end
  end
end
