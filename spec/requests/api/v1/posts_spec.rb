require 'rails_helper'

RSpec.describe Api::V1::PostsController, type: :controller do
  describe "GET #index" do
    it "returns a list of posts" do
      post1 = create(:post)
      post2 = create(:post)
      get :index
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).size).to eq(2)
    end
  end

  describe "GET #show" do
    it "returns the specified post" do
      post = create(:post)
      get :show, params: { id: post.id }
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)["id"]).to eq(post.id)
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "creates a new post" do
        expect {
          post :create, params: { post: { content: "Test content", author_id: 1 } }
        }.to change(Post, :count).by(1)
        expect(response).to have_http_status(:created)
      end
    end

    context "with invalid attributes" do
      it "does not create a new post" do
        expect {
          post :create, params: { post: { content: nil, author_id: 1 } }
        }.not_to change(Post, :count)
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PATCH #update" do
    it "updates the specified post" do
      post = create(:post)
      patch :update, params: { id: post.id, post: { content: "Updated content" } }
      expect(response).to have_http_status(:success)
      expect(post.reload.content).to eq("Updated content")
    end
  end

  describe "DELETE #destroy" do
    it "destroys the specified post" do
      post = create(:post)
      expect {
        delete :destroy, params: { id: post.id }
      }.to change(Post, :count).by(-1)
      expect(response).to have_http_status(:no_content)
    end
  end
end
