FactoryBot.define do
  factory :friendship do
    association :user, factory: :user
    association :friend, factory: :user
    status { :pending }
    requester_id { :user1 }

    trait :accepted do
      status { :accepted }
    end
  end
end
