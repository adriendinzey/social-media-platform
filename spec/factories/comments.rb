FactoryBot.define do
  factory :comment do
    content { "This is a test comment." }
    association :author, factory: :user
    association :post
  end
end
