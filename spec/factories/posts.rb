FactoryBot.define do
  factory :post do
    content { "This is a test post." }
    association :author, factory: :user
  end
end
