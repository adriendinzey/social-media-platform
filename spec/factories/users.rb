# spec/factories/users.rb
FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "user#{n}" }
    password { Faker::Internet.password }
    bio { Faker::Lorem.sentence }
  end
end
